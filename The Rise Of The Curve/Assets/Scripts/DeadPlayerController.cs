﻿using UnityEngine;
using System.Collections;

public class DeadPlayerController : MonoBehaviour
{
    public float speedRotate;
    public float angle;

    void FixedUpdate()
    {
        Quaternion target = Quaternion.Euler(0,0, angle);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * speedRotate);
        transform.position = new Vector3(transform.position.x, transform.position.y - 0.025f, 0);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BotController : MonoBehaviour {

    public float speedX;
    public float speedY;
    public float tilt;

    Rigidbody2D rb;
    private float h;

    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex % 2 != 1)
        {
            rb = GetComponent<Rigidbody2D>();
            h = -1f;
            StartCoroutine(BotReGo());
        }
    }

    IEnumerator BotReGo()
    {
        while (true)
        {
            Go();
            yield return new WaitForSeconds(Random.Range(0.5f, 1f));
        }
    }

    void FixedUpdate()
    {
        if (rb)
        {
            rb.velocity = new Vector3(rb.velocity.x, speedY, 0);
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);
        }
    }

    public void Go()
    {        
        h = -h;
        rb.velocity = new Vector3(0, speedY, 0);
        rb.AddForce(new Vector3(h * speedX, 0, 0));
    }
}

﻿using UnityEngine;
using System.Collections;

public class CameraSizeController : MonoBehaviour
{
    float changeWait;
    float rand;
    float origin = 4.4f;

    bool canChange = false;
    bool canSize = false;
    bool dead = false;

    float rememberSign;

    Camera cameraComp;

    void Start()
    {
        //rememberSign = DataBase.ReturnValue(rememberSign, "rememberSign");
        rememberSign = DataBase.ReturnFloatValue("rememberSign");
        cameraComp = GetComponent<Camera>();
        cameraComp.orthographicSize *= rememberSign;
    }

    IEnumerator ChangeSize()
    {
        while (true)
        {
            yield return new WaitForSeconds(changeWait);
            canChange = !canChange;
            rand = Random.Range(4.2f, 6.5f);
            changeWait = Random.Range(3, 10);
        }
    }

    public void GotReady()
    {
        canSize = true;
        changeWait = Random.Range(1, 2);
        StartCoroutine(ChangeSize());
    }

    public void ChangeMySign()
    {
        rememberSign = -rememberSign;
        cameraComp.orthographicSize *= -1;
        DataBase.Save(rememberSign, "rememberSign");

    }

    void FixedUpdate()
    {
        if (canSize && canChange)
        {
            if (rememberSign * cameraComp.orthographicSize < rand - 0.1)
            {
                cameraComp.orthographicSize += rememberSign * 0.005f;
            }
            if (rememberSign * cameraComp.orthographicSize > rand)
            {
                cameraComp.orthographicSize -= rememberSign * 0.005f;
            }
        }

        if (dead)
        {
            if (rememberSign * cameraComp.orthographicSize < origin)
            {
                cameraComp.orthographicSize += rememberSign * 0.01f;
            }
            if (rememberSign * cameraComp.orthographicSize > origin + 0.1f)
            {
                cameraComp.orthographicSize -= rememberSign * 0.01f;
            }
        }
    }

    public void DoDead()
    {
        dead = true;
        canSize = false;
    }

}

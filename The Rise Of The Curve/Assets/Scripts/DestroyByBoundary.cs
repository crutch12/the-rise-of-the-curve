﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour
{
    public GameObject walls;
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Wall")
        {
            Instantiate(walls, new Vector3(other.transform.position.x, other.transform.position.y + 45f, other.transform.position.z), Quaternion.Euler(0, 0, 0));
        }
        Destroy(other.gameObject);
    }
}
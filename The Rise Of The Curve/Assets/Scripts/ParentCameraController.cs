﻿using UnityEngine;
using System.Collections;

public class ParentCameraController : MonoBehaviour
{
    Transform target;

    float transY;

    bool needMoveUp;
    void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
        transY = transform.position.y;
    }

    void FixedUpdate()
    {
        if (target)
        {
            transform.position = new Vector3(transform.position.x, target.position.y + transY, transform.position.z);
        }
        if (needMoveUp && transY < 0.5)
        {
            transY += 0.02f;
        }
    }

    public void CameraMoveUp()
    {
        needMoveUp = true;
    }
}

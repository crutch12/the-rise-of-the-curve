﻿using UnityEngine;
using System.Collections;

public class TurnSide : MonoBehaviour {

    public float mnojit;
    public int maxRandomValue;

    private Rigidbody2D rb;
    private int diff;

	void Start ()
    {
        if (name == "Left")
        {
            diff = 1;
            Turn(diff, Random.Range(0, maxRandomValue));
        }

        if (name == "Right")
        {
            diff = -1;
            Turn(diff, Random.Range(0, maxRandomValue));
        }
	}
	
	void Turn (int diff, float rand)
    {
        rb = GetComponent<Rigidbody2D>();
        if (rand > 1)
        {
            float firstPosX = transform.position.x;
            float vinos = Random.Range(3.5f, 7);
            float posX = firstPosX - diff * vinos;
            Vector3 pos = new Vector3(posX, transform.position.y, transform.position.z);
            transform.position = pos;
            rb.velocity = new Vector3(diff * vinos / mnojit, 0, 0);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            rb.velocity = Vector3.zero; //остановить левую/правую платформу при врезании
        }
    }
}

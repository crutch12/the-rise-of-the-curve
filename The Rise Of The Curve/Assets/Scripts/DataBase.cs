﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class DataBase : MonoBehaviour
{
    void Awake()
    {
        if (!Directory.Exists(Inis.GetFullPath("")))
        {
            Directory.CreateDirectory(Inis.GetFullPath(""));
        }

        if (!File.Exists(Inis.GetFullPath("rememberSign.ini")))
        {
            Dictionary<string, string> dataToSave = new Dictionary<string, string>();
            dataToSave.Add("rememberSign", 1.ToString());
            Inis.Save("rememberSign.ini", dataToSave);
        }

        if (!File.Exists(Inis.GetFullPath("money.ini")))
        {
            Dictionary<string, string> dataToSave = new Dictionary<string, string>();
            dataToSave.Add("money", 0.ToString());
            Inis.Save("money.ini", dataToSave);
        }

        if (!File.Exists(Inis.GetFullPath("soundVolume.ini")))
        {
            Dictionary<string, string> dataToSave = new Dictionary<string, string>();
            dataToSave.Add("soundVolume", 1.ToString());
            Inis.Save("soundVolume.ini", dataToSave);
        }
    }

    public static float ReturnFloatValue(string name)
    {
        float rwc;
        Dictionary<string, string> loadedData = Inis.Load(name + ".ini");
        //Dictionary<string, string> loadedData = Ini.Load(Application.persistentDataPath + "/Inis/" + name + ".ini");
        rwc = System.Convert.ToInt32(loadedData[name]);
        return rwc;
    }

    public static int ReturnIntValue(string name)
    {
        int rwc;
        Dictionary<string, string> loadedData = Inis.Load(name + ".ini");
        //Dictionary<string, string> loadedData = Ini.Load(Application.persistentDataPath + "/Inis/" + name + ".ini");
        rwc = System.Convert.ToInt32(loadedData[name]);
        return rwc;
    }

    public static string ReturnStringValue(string name)
    {
        string rwc;
        Dictionary<string, string> loadedData = Inis.Load(name + ".ini");
        //Dictionary<string, string> loadedData = Ini.Load(Application.persistentDataPath + "/Inis/" + name + ".ini");
        rwc = System.Convert.ToString(loadedData[name]);
        return rwc;
    }

    public static void Save(float rwc, string name)
    {
        Dictionary<string, string> dataToSave = new Dictionary<string, string>();
        dataToSave.Add(name, rwc.ToString());
        Inis.Save(name + ".ini", dataToSave);
        //Ini.Save(Application.persistentDataPath + "/Inis/" + name + ".ini", dataToSave);
        //Debug.Log(Application.persistentDataPath);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    public float speedX;
    public float speedY;
    public float tilt;

    Rigidbody2D rb;
    private GameController gameController;
    private GameObject repeatFullEnemy;

    private float h;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();

        h = -1f;

        rb.velocity = new Vector3(rb.velocity.x, speedY, 0);
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            speedY++;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            speedY--;
        }

        //==================БЫВШИЙ ПОВОРОТ=====================
        //transform.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);
        //=====================================================
    }

    void Update()
    {
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);
    }

    public void Go()
    {
        h = -h;
        rb.velocity = new Vector3(0, speedY, 0);
        rb.AddForce(new Vector3(h * speedX, 0, 0));
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "FullEnemy")
        {
            if (repeatFullEnemy != other.gameObject)
            {
                gameController.AddScore();
            }
            repeatFullEnemy = other.gameObject;
        }
    }
}

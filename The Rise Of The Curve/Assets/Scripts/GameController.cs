﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using EZCameraShake;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject enemyLight;
    public GameObject enemyDark;

    public float spawnValue = 0.9f;
    public float spawnWait = 1f;
    public float startWait = 1f;

    GameObject mainCameraControl;
    GameObject player;
    GameObject trail;
    GameObject blurSprite;
    GameObject parentCamera;

    Transform playerTransform;

    PlayerController playerController;

    Text scoreText;
    Text gameOverText;
    Text moneyText;

    Button restartButton;
    Button controlButton;
    Button startGameButton;
    Button pauseButton;
    Button playButton;
    Button soundOnButton;
    Button soundOffButton;

    AudioSource musicBefore;
    AudioSource musicInPlay;

    bool musicBeforePaused = false;
    bool musicInPlayPaused = false;

    Canvas canvasPlay;
    Canvas canvasMenu;
    Canvas canvasGameOver;
    Canvas canvasPause;
    Canvas canvasSettings;

    bool settingsOpenedInPause;
    bool settingsOpenedInMenu;
    bool settingsOpenedInGameOver;

    bool playGame = false;
    int score;
    int money;

    int soundVolume;


    void Start()
    {
        //if ()
        /////////////////////////////////////////////////////////////
        mainCameraControl = GameObject.FindWithTag("MainCamera");
        player = GameObject.FindWithTag("Player");
        trail = GameObject.FindWithTag("Trail");
        blurSprite = GameObject.FindWithTag("Blur");
        parentCamera = GameObject.FindWithTag("ParentCamera");

        playerTransform = player.transform;

        playerController = player.GetComponent<PlayerController>();

        scoreText = GameObject.FindWithTag("Score").GetComponent<Text>();
        gameOverText = GameObject.FindWithTag("GameOver").GetComponent<Text>();
        moneyText = GameObject.FindWithTag("Money").GetComponent<Text>();

        restartButton = GameObject.FindWithTag("Restart").GetComponent<Button>();
        controlButton = GameObject.FindWithTag("BCont").GetComponent<Button>();
        //startGameButton = GameObject.FindWithTag("SGB").GetComponent<Button>();
        pauseButton = GameObject.FindWithTag("Pause").GetComponent<Button>();
        playButton = GameObject.FindWithTag("Play").GetComponent<Button>();
        soundOnButton = GameObject.FindWithTag("SoundOn").GetComponent<Button>();
        soundOffButton = GameObject.FindWithTag("SoundOff").GetComponent<Button>();

        musicBefore = GameObject.FindWithTag("MusicBefore").GetComponent<AudioSource>();
        musicInPlay = GameObject.FindWithTag("MusicInPlay").GetComponent<AudioSource>();

        canvasPlay = GameObject.FindWithTag("CanvasPlay").GetComponent<Canvas>();
        canvasMenu = GameObject.FindWithTag("CanvasMenu").GetComponent<Canvas>();
        canvasGameOver = GameObject.FindWithTag("CanvasGameOver").GetComponent<Canvas>();
        //canvasPause = GameObject.FindWithTag("CanvasPause").GetComponent<Canvas>();
        canvasSettings = GameObject.FindWithTag("CanvasSettings").GetComponent<Canvas>();
        /////////////////////////////////////////////////////////////////////////

        //if (DataBase.ReturnValue(soundVolume, "soundVolume") == 0)
        if (DataBase.ReturnIntValue("soundVolume") == 0)
            {
            AudioListener.volume = 0;
            soundOffButton.gameObject.SetActive(false);
            soundOnButton.gameObject.SetActive(true);
        }
        else
        {
            AudioListener.volume = 1;
            soundOffButton.gameObject.SetActive(true);
            soundOnButton.gameObject.SetActive(false);
        }

        score = 0;
        scoreText.text = "" + score;
        //money = DataBase.ReturnValue(money, "money");
        money = DataBase.ReturnIntValue("money");
        moneyText.text = "" + money;

        //gameOverText.enabled = false;

        TrailRenderer tr = trail.GetComponent<TrailRenderer>();
        tr.sharedMaterial = tr.materials[0];

        CloseCanvasGameOver();
        CloseSettings();

        restartButton.interactable = false;

        pauseButton.gameObject.SetActive(true);
        playButton.gameObject.SetActive(false);

        if (SceneManager.GetActiveScene().buildIndex % 2 == 0)
            {
            //canvasPlay.gameObject.SetActive(false);
            //canvasMenu.gameObject.SetActive(true);

            CloseCanvasPlay();
            OpenMenu();
        }

        if (SceneManager.GetActiveScene().buildIndex % 2 == 1)
        {
            //canvasPlay.gameObject.SetActive(true);
            //canvasMenu.gameObject.SetActive(false);

            OpenCanvasPlay();
            CloseMenu();

            blurSprite.SetActive(false);
        }

    }

    IEnumerator SpawnWaves()
    {
        //yield return new WaitForSeconds(startWait);
        while (true)
        {
            if (playerTransform)
            {
                if (Random.Range(0f, 2.1f) < 1.5f)
                {
                    Vector3 spawnPosition = new Vector3(Random.Range(-spawnValue, spawnValue), playerTransform.position.y + enemyLight.transform.position.y, enemyLight.transform.position.z);
                    Quaternion spawnRotation = Quaternion.identity; //Quaternion.identity - нулевой разворот (это наше начало)
                    Instantiate(enemyLight, spawnPosition, spawnRotation);
                }
                else
                {
                    Vector3 spawnPosition = new Vector3(Random.Range(-spawnValue, spawnValue), playerTransform.position.y + enemyDark.transform.position.y, enemyDark.transform.position.z);
                    Quaternion spawnRotation = Quaternion.identity; //Quaternion.identity - нулевой разворот (это наше начало)
                    Instantiate(enemyDark, spawnPosition, spawnRotation);
                }

                yield return new WaitForSeconds(spawnWait);
            }
            else
            {
                break;
            }
        }
    }

    IEnumerator ShowRestartButton()
    {
        yield return new WaitForSeconds(1);
        restartButton.interactable = true;
    }

    public void StartGame()
    {
        //canvasMenu.gameObject.SetActive(false);
        //canvasPlay.gameObject.SetActive(true);
        CloseMenu();
        OpenCanvasPlay();
        blurSprite.SetActive(false);

        DestroyBot();

        parentCamera.GetComponent<ParentCameraController>().CameraMoveUp();

    }

    public void StartPlay()
    {
        if (playGame == true)
        {
            //OnceShakeCamera(2f, 2f, 0f, 0.05f);
            playerController.Go();
        }
        else
        {
            playGame = true;
            OnceShakeCamera(9f, 15f, 0f, 1.1f);
            StartEnemies();
            StopSound(musicBefore);
            PlaySound(musicInPlay);
            ChangeTrail();
            scoreText.color = Color.red;
            playerController.Go();

            mainCameraControl.GetComponent<CameraSizeController>().GotReady();
        }
    }

    public void AddScore()
    {
        score++;
        scoreText.text = "" + score;

        money++;
        moneyText.text = "" + money;

        DataBase.Save(money, "money");
    }

    public void GameOver()
    {

        OpenCanvasGameOver();

        //controlButton.gameObject.SetActive(false);

        //blurSprite.GetComponent<SpriteRenderer>().color = new Color(0.973f, 1.000f, 0.816f, 0.353f); //yellow
        blurSprite.GetComponent<SpriteRenderer>().color = Color.white;
        blurSprite.SetActive(true);
        blurSprite.GetComponent<BlurController>().DoBlur();

        //gameOverText.text = "Game Over";
        //canvasGameOver.gameObject.SetActive(true);

        OnceShakeCamera(6f, 9f, 0f, 0.7f);

        mainCameraControl.GetComponent<CameraSizeController>().DoDead();

        pauseButton.gameObject.SetActive(false);
        playButton.gameObject.SetActive(false);

        //StartCoroutine(ShowRestartButton());
    }

    public void RestartGame() //вызывается нажатием кнопки Press for Restart! (поэтому public)
    {
        //if (System.Convert.ToInt32(Application.loadedLevel) / 2 == 1)
        //{

        //}
        //Application.LoadLevel("2");
        SceneManager.LoadScene(1);

    }

    public void LoadFirstScene()
    {
        SceneManager.LoadScene(0);
    }

    public void OpenSettings()
    {
        canvasSettings.gameObject.SetActive(true);
    }

    public void CloseSettings()
    {
        canvasSettings.gameObject.SetActive(false);
    }

    public void OpenPause()
    {
        blurSprite.SetActive(true);

        Time.timeScale = 0;
        PauseMusic();

        pauseButton.gameObject.SetActive(false);
        playButton.gameObject.SetActive(true);

        controlButton.gameObject.SetActive(false);
    }

    public void ClosePause()
    {
        blurSprite.SetActive(false);

        Time.timeScale = 1;
        UnpauseMusic();

        pauseButton.gameObject.SetActive(true);
        playButton.gameObject.SetActive(false);

        controlButton.gameObject.SetActive(true);
    }

    public void OpenCanvasPlay()
    {
        canvasPlay.gameObject.SetActive(true);
    }

    public void CloseCanvasPlay()
    {
        canvasPlay.gameObject.SetActive(false);
    }

    public void OpenMenu()
    {
        canvasMenu.gameObject.SetActive(true);
    }

    public void CloseMenu()
    {
        canvasMenu.gameObject.SetActive(false);
    }

    public void OpenCanvasGameOver()
    {
        //gameOverText.enabled = true;
        controlButton.gameObject.SetActive(false);

        gameOverText.text = "Game Over";
        canvasGameOver.gameObject.SetActive(true);

        //pauseButton.gameObject.SetActive(false);

        StartCoroutine(ShowRestartButton());
    }

    public void CloseCanvasGameOver()
    {
        canvasGameOver.gameObject.SetActive(false);
    }

    public void OpenSettingsInMenu()
    {
        OpenSettings();
        CloseMenu();
        settingsOpenedInMenu = true;
        settingsOpenedInPause = false;
        settingsOpenedInGameOver = false;
    }

    public void OpenSettingsInPause()
    {
        OpenSettings();
        //canvasPause.gameObject.SetActive(false);
        settingsOpenedInMenu = false;
        settingsOpenedInPause = true;
        settingsOpenedInGameOver = false;
    }

    public void OpenSettingsInGameOver()
    {
        OpenSettings();
        CloseCanvasGameOver();
        settingsOpenedInMenu = false;
        settingsOpenedInPause = false;
        settingsOpenedInGameOver = true;
    }

    public void WellSettings()
    {
        if (settingsOpenedInMenu)
        {
            OpenMenu();
        }
        else if (settingsOpenedInPause)
        {

        }
        else if (settingsOpenedInGameOver)
        {
            OpenCanvasGameOver();
        }

        CloseSettings();
    }

    public void FlipCamera()
    {
        //mainCameraControl.GetComponent<Camera>().orthographicSize = -mainCameraControl.GetComponent<Camera>().orthographicSize;
        mainCameraControl.GetComponent<CameraSizeController>().ChangeMySign();
    }

    public void SoundOn()
    {
        soundVolume = 1;
        DataBase.Save(soundVolume, "soundVolume");
        AudioListener.volume = soundVolume;
        //AudioListener.pause = false;
        soundOffButton.gameObject.SetActive(true);
        soundOnButton.gameObject.SetActive(false);

    }

    public void SoundOff()
    {
        soundVolume = 0;
        DataBase.Save(soundVolume, "soundVolume");
        AudioListener.volume = soundVolume;
        //AudioListener.pause = true;
        soundOffButton.gameObject.SetActive(false);
        soundOnButton.gameObject.SetActive(true);

    }

    void DestroyBot()
    {
        Destroy(player.GetComponent<BotController>());
    }

    void StartEnemies()
    {
        StartCoroutine(SpawnWaves());
    }

    void PlaySound(AudioSource au)
    {
        au.Play();
    }
    void PauseSound(AudioSource au)
    {
        au.Pause();
    }
    void StopSound(AudioSource au)
    {
        au.Stop();
    }

    void UnpauseMusic()
    {
        if (musicInPlayPaused == true)
        {
            PlaySound(musicInPlay);
            musicInPlayPaused = false;
        }
        else if (musicBeforePaused == true)
        {
            PlaySound(musicBefore);
            musicBeforePaused = false;
        }
    }

    void PauseMusic()
    {
        if (musicInPlay.isPlaying)
        {
            PauseSound(musicInPlay);
            musicInPlayPaused = true;
        }
        else if (musicBefore.isPlaying)
        {
            PauseSound(musicBefore);
            musicBeforePaused = true;
        }
    }

    void ChangeTrail()
    {
        TrailRenderer tr = trail.GetComponent<TrailRenderer>();
        tr.sharedMaterial = tr.materials[1];
    }

    void OnceShakeCamera(float magnitude, float roughness, float fadeInTime, float fadeOutTime)
    {
        //mainCameraController.GetComponent<CameraShaker>().ShakeItOnce(8.5f, 9f, 0f, 1.4f);
        mainCameraControl.GetComponent<CameraShaker>().ShakeItOnce(magnitude, roughness, fadeInTime, fadeOutTime);
    }

    void StartShakeCamera(float magnitude, float roughness, float fadeInTime)
    {
        mainCameraControl.GetComponent<CameraShaker>().StartShakeIt(magnitude, roughness, fadeInTime);
    }
}

﻿using UnityEngine;
using System.Collections;

public class BlurController : MonoBehaviour {

    private bool doBlur;
    private float blurF;

    void FixedUpdate()
    {
        if (doBlur && blurF < 0.5)
        {
            blurF += 0.01f;
            GetComponent<SpriteRenderer>().color = new Color(0.682f, 0.486f, 0.486f, blurF); //red
        }
    }

    public void DoBlur()
    {
        doBlur = true;
    }

    //public void DestroyBlur()
    //{
    //    blurF = 0;
    //    gameObject.GetComponent<Renderer>().material.SetFloat("_BumpAmt", blurF);

    //}

    //public void CreateBlur()
    //{
    //    blurF = 128;
    //    gameObject.GetComponent<Renderer>().material.SetFloat("_BumpAmt", blurF);
    //}
}
